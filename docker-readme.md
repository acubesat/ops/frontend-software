Docker is a way to run Linux app without needing to download any virtual machine.
It is fast, simple to use, and doesn't need much space or hardware. 

[Walktrhough video for installation](https://www.youtube.com/watch?v=2ezNqqaSjq8) 

## Download docker
[installation](https://www.docker.com/)  
Note: You might to restart your PC 

## Download ubuntu through WSL 
- Open windows PowerShell and run it as an administrator 

 ![powershell](application_screenshots\powershell.png)

-After that type: `wsl --install -d Ubuntu`
It will start downloading ubuntu to your PC    
A window will appear that will ask you to type in your username and password  
After that Ubuntu will have been downloaded to your PC 

## Docker WSL integration 

to run docker through ubuntu you will need to enable this option through Docker 
1. Go to docker and click settings 
![docker_settings](application_screenshots\docker_settings.png)
2. Click resources 
3. Click WSL integration 
4. Enable integration with additional distros for ubuntu 
if you type docker in your ubuntu command line it will appear all docker commands available 

## Use the docker image to open the frontend app 

[walkthrough video](https://www.youtube.com/watch?v=etA5xiX5TCA&t=585s)

To run the frontend application you will need to create an image of the application 
You will first need to install the docker extension for vscode 

![Screenshot_3](application_screenshots\extension_docker.png)

In order to run the app we have already created a `Dockerfile` inside our app. You will need to build your image by selecting the command `build image`
![build_image](application_screenshots\build_image.png)
You will need to pick the app you want to build as well as a name for your image.  
This process might take some time. Docker will start downloading all the necessary tools to run your application. 

After that is completed you type in the run command with `run image`.  
When building the image, using `run` you need to add the following port flag:

```
-p 8000:8000
```

This exposes the internal `8000` port of the container to be accessible via the `8000` port of your os. Hope it makes sense.
![run_command](application_screenshots\run_command.png)

when you finish go to your browser to localhost:8080
your app will be hosted there 

## Possible issues  

When your PC has restarted open Docker 
You might encounter this below issue 


![INCOMPLETE_ISNTALLATION](application_screenshots\INCOMPLETE_ISNTALLATION.png)

to solve it follow the instruction in [this site](https://learn.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)


## Docker installation for Ubuntu

In order to install Docker on Ubuntu, open a terminal and follow these steps:
1. Update the package index and install the necessary packages:
```bash
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release
```
2. Add the Docker GPG key: 
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
3. Add the Docker repository to your system: 
```bash
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
4. Update the package index to include the Docker repository: 
```bash
sudo apt update
```
5. Install Docker: 
```bash
sudo apt install docker-ce docker-ce-cli containerd.io
```
6. Verify Docker's installation: 
```bash
docker version
```
You have now successfully installed docker on your system, but you will most likely get some permission problems if you try to build and run an image as mentioned on the WSL section. In order to resolve the permission problems run the following commands:  
1. Change the ownership of the Docker socket file: 
```bash
sudo chown $USER:$USER /var/run/docker.sock
```
2. Add your user to the docker group: 
```bash
sudo usermod -aG docker $USER
```
3. Change the permissions of the Docker socket file: 
```bash
sudo chmod 666 /var/run/docker.sock
```
4. Log out of your current session and log back in for the changes to take effect.

Now all you will need to do to build and run a docker image is install the necessary dependencies on VsCode and follow the instructions on the [WSL section](#use-the-docker-image-to-open-the-frontend-app). Then, type `localhost` on your preferred browser and the image should be running there.