import { Component, HostListener, OnDestroy, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NavigationEnd, Router } from "@angular/router";
import { NotificationService } from "./services/notification.service";
import { WebsocketService } from "./services/websocket.service";
import { DataserviceService } from "./services/dataservice.service";
import { HttpService } from "./services/http.service";
import { ListInstancesResponse } from "./Interfaces/instances";
import { Subscription, lastValueFrom } from "rxjs";
import { MatDialog } from '@angular/material/dialog';
import { ChangeInstanceDialogComponent } from "./components/change-instance-dialog/change-instance-dialog.component";
import { PacketReceptionService } from "./services/packet-reception.service";
import { now } from "underscore";
import { Arc } from "d3-shape";
import { ArchivePacketsService } from "./services/archive-packets.service";
import { InstanceHandlerService } from "./services/instance-handler.service";


export interface DialogData {
  instance: 'OBC' | 'ADCS' | 'COMMS';
}

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit, OnDestroy {
  @HostListener('window:beforeunload')
  refresh(){
    this.ngOnDestroy();
  }
  homeUrl: boolean = true;
  graphsView!: boolean;
  time!: Date;
  utcTime!: Date;
  instances!:ListInstancesResponse;
  currentInstance!: string;

  instanceSubscription!: Subscription;
  notificationSubscription!: Subscription;
  currentGreekTimeSubscription!: Subscription;
  currentUTCTimeSubscription!: Subscription;

  constructor(
    private wss: WebsocketService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dataService: DataserviceService,
    private httpService: HttpService,
    public dialog: MatDialog,
    private packets: PacketReceptionService,
    private archivePackets: ArchivePacketsService,
    private instanceHandlerService: InstanceHandlerService
  ) {
    this.notificationSubscription = this.notificationService.notification$.subscribe((message) => {
      this.snackBar.open(message, "OK", { duration: 3000 });
    });
    this.instanceSubscription = this.instanceHandlerService
    .getInstance()
    .subscribe((instance) => (this.currentInstance = instance));
    this.router.navigate([""]);
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url == "/") {
          this.homeUrl = true;
        } else {
          this.homeUrl = false;
        }
      }
    });
  }

  async ngOnInit(): Promise<void> {
    this.currentGreekTimeSubscription = this.dataService.currentgreekTime.subscribe((time) => {
      this.time = time;
    });
    this.currentUTCTimeSubscription = this.dataService.currentUTCTime.subscribe((time) => {
      this.utcTime = time;
    });

    try {
      await this.wss.newConnection();
    } catch (error) {
      console.error("Error occurred while connecting:", error);
    }
    this.packets.initiatePacketReception();
    this.archivePackets.setTimeofPacketReception();

    setInterval(() => {
      this.time = new Date();
      this.dataService.changeTime(this.time);
    }, 1000);
    this.instances= await lastValueFrom(this.httpService.getInstances());
    // window.onbeforeunload = () => this.ngOnDestroy();  
  }

  onInstanceClick(instanceClick: string) {
    const dialogRef = this.dialog.open(ChangeInstanceDialogComponent, {   
      data: {
        instance: instanceClick,
      },
    });
  }

  ngOnDestroy(): void {
    this.wss.unsubscribeFromInstanceHandlerMethod();
    this.httpService.unsubscribeFromInstanceHandlerMethod();
    if(this.notificationSubscription) this.notificationSubscription.unsubscribe();
    if(this.currentGreekTimeSubscription) this.currentGreekTimeSubscription.unsubscribe();
    if(this.currentUTCTimeSubscription) this.currentUTCTimeSubscription.unsubscribe();
    if(this.instanceSubscription) this.instanceSubscription.unsubscribe();
  }
  
}
