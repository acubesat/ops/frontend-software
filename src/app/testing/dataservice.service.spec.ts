import { TestBed } from '@angular/core/testing';

import { DataserviceService } from '../services/dataservice.service';

describe('DataserviceService', () => {
  let service: DataserviceService;

  beforeEach(() => {
    service = new DataserviceService(); 
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("should change queries", () => {
    service.changeQueries("new start", "new stop","new count");
    service.currentStart.subscribe((start)=> expect(start).toBe("new start"));
    service.currentStop.subscribe((stop) => expect(stop).toBe("new stop"));
    service.currentCount.subscribe((count) => expect(count).toBe("new count"));
  })

  it("should change name", () => {
    service.changeParameterName("new name");
    service.currentName.subscribe((value) => expect(value).toBe("new name"));
  })

  it("should change view", () => {
    service.currentNgIF.subscribe((ngif) => expect(ngif).toBeTrue());
  })

  it("should equal 10", () => {
    const enumElement = [{
      value: "10",
      label: "new label",
      description: "new description"
    },]
    service.checkEnumMap(enumElement);
    expect(service.enumaratedMap.get("new label")).toEqual(10);
  })

});
