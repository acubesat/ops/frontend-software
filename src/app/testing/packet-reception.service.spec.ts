import { TestBed } from '@angular/core/testing';

import { PacketReceptionService } from '../services/packet-reception.service';

describe('PacketReceptionService', () => {
  let service: PacketReceptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PacketReceptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
