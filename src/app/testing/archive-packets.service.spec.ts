import { TestBed } from '@angular/core/testing';

import { ArchivePacketsService } from '../services/archive-packets.service';

describe('ArchivePacketsService', () => {
  let service: ArchivePacketsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArchivePacketsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
