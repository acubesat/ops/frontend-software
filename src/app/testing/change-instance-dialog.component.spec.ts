import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeInstanceDialogComponent } from '../components/change-instance-dialog/change-instance-dialog.component';

describe('ChangeInstanceDialogComponent', () => {
  let component: ChangeInstanceDialogComponent;
  let fixture: ComponentFixture<ChangeInstanceDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeInstanceDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangeInstanceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
