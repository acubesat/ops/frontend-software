import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PacketExtractComponent } from '../components/packet-extract/packet-extract.component';

describe('PacketExtractComponent', () => {
  let component: PacketExtractComponent;
  let fixture: ComponentFixture<PacketExtractComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PacketExtractComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PacketExtractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
