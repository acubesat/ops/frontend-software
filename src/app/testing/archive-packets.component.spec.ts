import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivePacketsComponent } from '../components/archive-packets/archive-packets.component';

describe('ArchivePacketsComponent', () => {
  let component: ArchivePacketsComponent;
  let fixture: ComponentFixture<ArchivePacketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchivePacketsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArchivePacketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
