import { TestBed } from '@angular/core/testing';

import { ExtractPacketService } from '../services/extract-packet.service';

describe('ExtractPacketService', () => {
  let service: ExtractPacketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExtractPacketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
