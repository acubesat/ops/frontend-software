import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';
import { BehaviorSubject, Observable, Subject, Subscription, takeUntil } from 'rxjs';
import { TmPacketReply, subscribePacketsData, SubscribePacketsService} from '../Interfaces/SubscribePackets';
import { ExtractPacketResponse } from '../Interfaces/ExtractPacketResponse';
import { ExtractPacketService } from './extract-packet.service';
import { InstanceHandlerService } from './instance-handler.service';


@Injectable({
  providedIn: 'root'
})
export class PacketReceptionService {

  packetDataSubscription: Subscription | undefined;
  extractedPacket!: ExtractPacketResponse;
  packetData: TmPacketReply[] = [];
  conectionCall!: number;
  subscribePacketRequest: SubscribePacketsService = {
    packetData: this.packetData
  };
  latestPacketTime: Date = new Date();
  yamcsInstanceName: string = "OBC";

  private unsubscribeFromInstanceHandler = new Subject();

  subscribePacketsServiceData = new BehaviorSubject<SubscribePacketsService>(this.subscribePacketRequest);

  constructor(private wss: WebsocketService, private extract_packet: ExtractPacketService, private instanceHandler: InstanceHandlerService) {
    this.instanceHandler
      .getInstance()
      .pipe(takeUntil(this.unsubscribeFromInstanceHandler))
      .subscribe((instanceName) => (this.yamcsInstanceName = instanceName));
   }

  /**
   * New packet request, listens for new packets and publishes the packet data
   */
  initiatePacketReception(): void {
    this.wss.newPacketsRequest();
    this.wss.listen();
    this.packetDataSubscription = subscribePacketsData.subscribe(async data => {
      if(this.conectionCall!=data.call || this.conectionCall== undefined){
      this.conectionCall=data.call;
      }
      this.extractedPacket = await this.extract_packet.exctractPacket(data.data.generationTime);
      data.data.id.name = this.extractedPacket.packetName;
      this.packetData.unshift(data);
      this.packetData = [...this.packetData];
      this.subscribePacketRequest = {
        packetData: this.packetData
      };
      this.latestPacketTime = data.data.generationTime;
      this.subscribePacketsServiceData.next(this.subscribePacketRequest);
    });
  }
  /**
   * Send a pakcet to cancel the websocket conection 
   */
  cancelPacketsConection():void{
    console.log( "cancel:"+this.conectionCall)
    this.wss.cancelCall(this.conectionCall);
  }

  /**
   * Returns an observable of the packet data
   * 
   * @returns an observable of the packet data
   */
  getPacketData(): Observable<SubscribePacketsService> {
    return this.subscribePacketsServiceData.asObservable();
  }


  getLatestPacketTime(): Date {
    return this.latestPacketTime;
  }

  unsubscribeFromInstanceHandlerMethod(){
    this.unsubscribeFromInstanceHandler.next('');
  }

  

}

