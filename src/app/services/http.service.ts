import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject, takeUntil, Observable, lastValueFrom } from "rxjs";
import { TimeSeries } from "../Interfaces/ArchiveParameters";
import { ListParametersResponse } from "../Interfaces/ListParameterResponse";
import { Ranges } from "../Interfaces/Ranges";
import { CommandHistoryEntry } from "../Interfaces/TcHistory";

import { yamcsBaseUrl } from "../Interfaces/GraphData";
import { InstanceHandlerService } from "./instance-handler.service";
import { ListCommandsResponse } from "../Interfaces/Telecommands";
import { ListInstancesResponse } from "../Interfaces/instances";
import { ListSpaceSystemsResponse } from "../Interfaces/ListSpaceSystemsResponse";
import { subscribeLinkData,LinkInfo } from "../Interfaces/subscribe-links-data";
import { ListDataLink } from "../Interfaces/list-data-link";
@Injectable({
  providedIn: "root",
})

/**
 * Responsible for sending http requests to yamcs
 */
export class HttpService {
  yamcsInstanceName: string = "OBC"; //default yamcs instance is OBC
  apiUrl = `${yamcsBaseUrl}/mdb/${this.yamcsInstanceName}/parameters/?limit=140`;
  archiveUrl = `${yamcsBaseUrl}/archive/${this.yamcsInstanceName}/parameters`;

  commandsUrl: string = `${yamcsBaseUrl}/mdb/${this.yamcsInstanceName}/commands`; //add ?noAbstract to get the Tcs we can send
  commandfoldersUrl = new BehaviorSubject<string>(
    `${yamcsBaseUrl}/mdb/${this.yamcsInstanceName}/space-systems`
  );

  private unsubscribeFromInstanceHandler = new Subject();

  unsubscribeFromInstanceHandlerMethod(){
    this.unsubscribeFromInstanceHandler.next('');
  }

  constructor(
    private http: HttpClient,
    private instanceHandler: InstanceHandlerService
  ) {
    this.instanceHandler
      .getInstance()
      .pipe(takeUntil(this.unsubscribeFromInstanceHandler))
      .subscribe((instanceName) => (this.yamcsInstanceName = instanceName));
  }

  getInstances(): Observable<ListInstancesResponse> {
    return this.http.get<ListInstancesResponse>(`${yamcsBaseUrl}/instances`);
  }

  getCommands(): Observable<ListCommandsResponse> {
    return this.http.get<ListCommandsResponse>(this.commandsUrl);
  }

  getTcResponse(id: string, instance: string): Observable<CommandHistoryEntry> {
    return this.http.get<CommandHistoryEntry>(
      `${yamcsBaseUrl}/archive/${instance}/commands/${id}`
    );
  }

  /***
   *
   * @returns an observable of the list of the parameters that yamcs contains
   */

  getParameters(): Observable<ListParametersResponse> {
    return this.http.get<ListParametersResponse>(this.apiUrl);
  }

  /**
   * @parameterName = the parameter's qualifiedName
   * @start = the time we want to start getting the samples
   * @stop = the stop time we want to stop getting the samples
   * @count = number of intervals to use, default is 500
   * @returns the samples of a specific parameter as an observable if that parameter is not an enumeration,
   * because enums do not have samples
   */

  getSamples(
    parameterName: string,
    start: string,
    stop: string,
    count: string
  ): Observable<TimeSeries> {
    let url =
      this.archiveUrl +
      parameterName +
      "/samples?" +
      "start=" +
      start +
      "&" +
      "stop=" +
      stop +
      "&" +
      "count=" +
      count;
    return this.http.get<TimeSeries>(url);
  }

  /**
    @parameterName = the parameter's qualifiedName
    @start = the time we want to start getting the samples
    @stop = the stop time we want to stop getting the samples
    @returns the ranges of a specific parameter if that parameter is an enumeration
    */

  getArchivedEnumSamples(
    parameterName: string,
    start: string,
    stop: string
  ): Observable<Ranges> {
    let url = this.archiveUrl.concat(
      parameterName,
      "/ranges?",
      "start=",
      start,
      "&" + "stop=",
      stop
    );
    return this.http.get<Ranges>(url);
  }
  getInstance(): string {
    return this.yamcsInstanceName;
  }

  async getCommandsFoldersResponse(): Promise<string[]> {
    let newfoldersUrl = `${yamcsBaseUrl}/mdb/${this.yamcsInstanceName}/space-systems`;
    this.commandfoldersUrl.next(newfoldersUrl);
    let spaceSystems = await lastValueFrom(
      this.http.get<ListSpaceSystemsResponse>(this.commandfoldersUrl.value)
    );
    let folders: string[] = [];
  
    spaceSystems.spaceSystems.forEach((spaceSystem) => {
      // Check if the name starts with "ST[" in order to keep only the folders that contain TCs
      if ((spaceSystem.name.startsWith("ST[") || spaceSystem.name.includes("pus-not-used")) && !spaceSystem.name.includes("ST[01]")) {
        folders.push(spaceSystem.name);
      }
    });
    return folders;
  }

  getLinks():Observable<ListDataLink>{
    return this.http.get<ListDataLink>(`${yamcsBaseUrl}/links/${this.yamcsInstanceName}`)
  }
  disableLink(linkName:string){
    this.http.post<LinkInfo>(`${yamcsBaseUrl}/links/${this.yamcsInstanceName}/${linkName}:disable`,"").subscribe();
    console.log("send disable");}

    enableLink(linkName:string){
      this.http.post<any>(`${yamcsBaseUrl}/links/${this.yamcsInstanceName}/${linkName}:enable`,"").subscribe();
      console.log("send enable");
    }
    resetLink(linkName:string){
      this.http.post<any>(`${yamcsBaseUrl}/links/${this.yamcsInstanceName}/${linkName}:resetCounters`,"").subscribe();
      console.log("send reset");

    }
}
