import { ListPacketsResponse, TmPacketData } from 'src/app/Interfaces/SubscribePackets';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, catchError, empty, lastValueFrom, map, of, takeUntil } from 'rxjs';
import { InstanceHandlerService } from './instance-handler.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { filter } from 'underscore';

@Injectable({
  providedIn: 'root'
})
export class ArchivePacketsService {

  private start = new BehaviorSubject<string>("");
  currentStart = this.start.asObservable();
  private stop = new BehaviorSubject<string>("");
  currentStop = this.stop.asObservable();
  packetData!: ListPacketsResponse;
  continuationToken: string = '';
  packetsDataArray: TmPacketData[] = [];
  response? : ListPacketsResponse;


  timeofPacketReception: Date = new Date();

  yamcsInstanceName: string = "OBC";

  private unsubscribeFromInstanceHandler = new Subject();
  
  constructor(private http: HttpClient,
    private snackBar: MatSnackBar,
    private instanceHandler: InstanceHandlerService) { 
      this.instanceHandler
      .getInstance()
      .pipe(takeUntil(this.unsubscribeFromInstanceHandler))
      .subscribe((instanceName) => (this.yamcsInstanceName = instanceName));
  }

  openSnackBar(message: string, action: string | undefined): void {
    /**
     * this is the help button in the 3rd step
     */
    this.snackBar.open(message, action, {
      duration: 5000,
      panelClass: "black-snackbar", // color of snackbar
    });
  }

  /**
   * Sets the time of packet reception
   */
  setTimeofPacketReception() {
    this.timeofPacketReception = new Date();
    this.http.get(`http://localhost:8090/api/archive/${this.yamcsInstanceName}/packets?limit=1&order=asc`).subscribe((data) => {
      this.packetData = data as ListPacketsResponse;
      this.timeofPacketReception = new Date(this.packetData.packet[0].generationTime);
    });
  }

  /**
   * Sends an archive packet query
   * 
   * @param start the start time
   * @param stop the stop time
   * @returns the archive packets
   */
  async sendPacketQuery(start: string, stop: string): Promise<TmPacketData[]> {
    let timeFrame = new Date(stop).getTime() - new Date(start).getTime();
    let maxCount = 5; // Maximum requests to be made
    let countFlag = false; // Flag to check if the time frame is more than 5 minutes
    if(timeFrame > 5*60*1000){
      //stop = new Date(new Date(start).getTime() + 5*60*1000).toISOString();
      countFlag = true;
    }
    console.log("Sending packet query")
    let counter = 0;
    while(!this.continuationToken != null || counter == 0 ){
      if(counter == 0){//if first iteration
        const tempResponse : any = await lastValueFrom(this.packetRequestFirst(start, stop));
        if(tempResponse == undefined || tempResponse == null || tempResponse == '' || tempResponse.packet == undefined || tempResponse.packet.length == 0){
          this.openSnackBar("No packets found", "OK");
          break;
        }
        else{
          console.log(timeFrame);
          if(timeFrame > 5*60*1000){
            this.openSnackBar("Time frame is more than 5 minutes, only the first 500 packets will be displayed", "OK");
          }
          this.response = tempResponse as ListPacketsResponse;
        }
      }
      else if(countFlag && counter == maxCount){//if time frame is more than 5 minutes and 5 requests have been made
        break;
      }
      else{
        this.response = await lastValueFrom(this.packetRequest(start, stop));
      }
      this.packetsDataArray = [...this.packetsDataArray, ...this.response.packet];
      if(this.response.continuationToken == null || this.response.continuationToken == '' || this.response.continuationToken == undefined){
        break;
      }
      this.continuationToken = this.response.continuationToken;
      if(!this.response){//if no packets are returned
        this.openSnackBar("No packets found", "OK");
        break;
      }
      this.response = undefined;
      counter++;
    }
    if(this.packetsDataArray.length > 500){
      this.packetsDataArray.splice(500); // Limit the number of packets to 500
      let snackBarMessage = "Packets found are more than 500, only the first 500 will be displayed. Packets received: " + this.packetsDataArray.length;
      this.openSnackBar(snackBarMessage, "OK");
    }
    return this.packetsDataArray;
  }

  /**
   * First archive packet request
   * 
   * @param start the start time
   * @param stop the stop time
   * @returns the packets
   */
  packetRequestFirst(start: string, stop: string): Observable<any> {
    return this.http.get<any>(`http://localhost:8090/api/archive/${this.yamcsInstanceName}/packets?start=${start}&stop=${stop}`).pipe(
      map(response => response),
        catchError(error => {
          console.error('Error fetching packets:', error);
          return new Observable<undefined>(observer => observer.next(undefined));
        })
    );
  }

  /**
   * Archive packet request
   * 
   * @param start the start time
   * @param stop the stop time
   * @returns the packets
   */
  packetRequest(start: string, stop: string): Observable<ListPacketsResponse> {
    return this.http.get<ListPacketsResponse>(`http://localhost:8090/api/archive/${this.yamcsInstanceName}/packets?start=${start}&stop=${stop}&next=${this.continuationToken}`);
  }

  unsubscribeFromInstanceHandlerMethod(){
    this.unsubscribeFromInstanceHandler.next('');
  }

  getPacketReceptionTime(): Date{
    return this.timeofPacketReception;
  }

  changeStop(stop: string) {
    this.stop.next(stop);
  }

  changeQueries(start: string, stop: string) {
    this.start.next(start);
    this.stop.next(stop);
  }
}
