import { TelemetryPacketHistoryComponent } from './../components/telemetry-packet-history/telemetry-packet-history.component';
import { Injectable } from "@angular/core";
import {
  UpdateSeries,
  graphData,
  Series,
  callMatchingMap,
  calls,
} from "src/app/Interfaces/GraphData";
import { InstanceHandlerService } from "./instance-handler.service";
import { subscribePacketsData } from '../Interfaces/SubscribePackets';
import { Subject, takeUntil,Observable } from 'rxjs';
import { subscribeLinkData } from '../Interfaces/subscribe-links-data';


@Injectable({
  providedIn: "root",
})
export class WebsocketService {
  yamcsInstanceName: string = "OBC";
  socket!: WebSocket;
  id: number = 1;
  dataid:number=1;

  private unsubscribeFromInstanceHandler = new Subject();

  constructor(private instanceHandler: InstanceHandlerService) {
    this.instanceHandler.getInstance().pipe(takeUntil(this.unsubscribeFromInstanceHandler)).subscribe((instanceName) => (this.yamcsInstanceName = instanceName));
  }

  unsubscribeFromInstanceHandlerMethod(){
    this.unsubscribeFromInstanceHandler.next('');
  }

  /**
   * Establish WebSoket connection.
   */
  newConnection(): Promise<void> {
    return new Promise((resolve, reject) => {
      /**Creates a websocket connection */
      if (!this.socket) {
        this.socket = new WebSocket("ws://localhost:8090/api/websocket");
      }

      /**Connection opened*/
      this.socket.onopen = function () {
        console.log("Connection opened");
        resolve();
      };

      // Connection failed
      this.socket.onerror = (error) => {
        reject(error);
      };
    });
  }

  /**
   * New parameter request to specified parameter name.
   *
   * @param parameterQualifiedName = The qualified name of the parameter used for the request
   */

  newParameterRequest(parameterQualifiedName: string): void {
    let parameterRequest = {
      type: "parameters",
      id: this.id,
      options: {
        instance: this.yamcsInstanceName,
        processor: "realtime",
        id: [{ name: parameterQualifiedName }],
      },
    };

    this.id++;

    if (this.socket.readyState == 1) {
      this.socket.send(JSON.stringify(parameterRequest));
      console.log("Sent parameter request");
    } else {
      console.log("WebSocket not connected");
    }
  }

  /**
   * New packet request to specified instance.
   */
  newPacketsRequest(): void {
    let packetsRequest = {
      type: "packets",
      id: 404,
      options: {
        instance: this.yamcsInstanceName,
        processor: "realtime",
      },
    };

    if (this.socket.readyState == 1 && this.yamcsInstanceName != "" ) {
      this.socket.send(JSON.stringify(packetsRequest));
      console.log("Sent packet request");
    } else {
      console.log("WebSocket not connected");
    }
  }

   // New Data Link request to specified name
   newDataLinkRequest(): void {
    this.dataid=this.id+this.dataid;
    let dataLinkRequest = {
      type: "links",
      id: this.dataid,
      options: {
        instance: this.yamcsInstanceName,
      },
    };

    this.dataid++;

    if (this.socket.readyState == 1) {
      this.socket.send(JSON.stringify(dataLinkRequest));
      console.log("Sent Datalink request");
    } else {
      console.log("WebSocket not connected");
    }
  }

  /**
   * Listens to the websocket connection for messages and parses the data.
   */
  listen(): void {
    var time: string;
    var callNumber: number = 0
    /**
     * The message event is fired when data is received through a WebSocket.
     */
    this.socket.onmessage = function (event) {
      var yamcsReply = JSON.parse(event.data);
      if (yamcsReply.type == "parameters") {
        /**
         * Checks if the call exists in the map.
         * If it doesn't, we add it, if it does, we don't.
         */
          if(yamcsReply.seq == 1 && !callMatchingMap.has(yamcsReply.call)){
            callMatchingMap.set(yamcsReply.call, yamcsReply.data.mapping[1].name);
            callNumber++;
            calls.push(yamcsReply.call);
          }
          /**
           * Checks if the parameter we're requesting has real-time values.
           * If we don't do this check, there will be erros at line 95
           * since there won't be a yamcsReply.data.values[0].
           * If it has real-time values, we extract the data and publish 
           * it to the graph-item component. We're also checking if the 
           * parameter is of an appropriate type, meaning integer or float, 
           * in order to create an ngx line chart.
           */
          if(yamcsReply.data.hasOwnProperty("values")){
            var parameterName: string = <string>callMatchingMap.get(yamcsReply.call); //gets the param name from the map according to the call's number
            time = yamcsReply.data.values[0].generationTime; //keep in mind that we might need to convert this if yamcs uses its custom time
            let realTime = new Date(time);
            let engType = Object.keys(yamcsReply.data.values[0].engValue)[1];
            let value = yamcsReply.data.values[0].engValue[engType];
            let engineeringType = yamcsReply.data.values[0].engValue.type;
            let isOfType: boolean;
            if(engineeringType == "ARRAY" || engineeringType == "ENUMERATED" || engineeringType == "TIMESTAMP" || engineeringType == "STRING"){
              isOfType = false;
            }else{
              isOfType = true;
            }
            const series: Series = {
              name: realTime,
              value: value,
            };

            const currentData: UpdateSeries = {
              name: parameterName,
              call: yamcsReply.call,
              isOfType: isOfType,
              value: series,
              type : engineeringType,
            };

            graphData.next(currentData);
          }
      }
      /**
       * If the reply is a packet, we publish it to the telemetry-packet-history component
       */
      else if(yamcsReply.type == "packets"){
        subscribePacketsData.next(yamcsReply);
      }
      /**
       * If the reply is a link, we publish it to the data-links component
       */
      else if(yamcsReply.type == "links" && yamcsReply.data.type=='UPDATE_ALL'){
        subscribeLinkData.next(yamcsReply);
      }
    };
  }

  cancelCall(index: number): void{
    let cancel = {
      "type": "cancel",
      "options":{
        "call": index
      }
    }
    this.socket.send(JSON.stringify(cancel));
  }

  /*
  errorHandler = (): Observable<Event> => {
    return new Observable(observer => {
      this.socket.onerror = (event: Event) => {
        console.log("An error has occured! : ", event);
        observer.next(event);
      };
    });
  };



  /** 
  close(): void{
    try{
      this.socket.close();
    } catch (error){
      console.log("Eror occured while attempting to terminate connection : " , error)
    }
  }
 */
}
