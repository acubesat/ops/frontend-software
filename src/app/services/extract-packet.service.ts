import { Injectable } from '@angular/core';
import { TmPacketReply, subscribePacketsData } from 'src/app/Interfaces/SubscribePackets';
import { ExtractPacketResponse } from 'src/app/Interfaces/ExtractPacketResponse';
import { yamcsBaseUrl } from "../Interfaces/GraphData";
import { InstanceHandlerService } from "./instance-handler.service";
import { HttpClient } from '@angular/common/http';
import { Subject, lastValueFrom, takeUntil } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExtractPacketService {

  gentime: Date = new Date();
  seqnum!: number;
  uri!: string;
  yamcsInstanceName: string = "OBC";
  extractedPacket!: ExtractPacketResponse;

  private unsubscribeFromInstanceHandler = new Subject();

  constructor(private instanceHandler: InstanceHandlerService, private http: HttpClient,) {
    this.instanceHandler
    .getInstance()
    .pipe(takeUntil(this.unsubscribeFromInstanceHandler))
    .subscribe((instanceName) => (this.yamcsInstanceName = instanceName));
    }

    /**
     * Extracts the packet specified and returns the extracted packet
     * 
     * @param packet 
     * @returns the extracted packet
     */
  async exctractPacket(packetGenerationTime: Date) : Promise<ExtractPacketResponse> {
    this.gentime = packetGenerationTime;
    this.seqnum = 0; //temporary until we can get the seqnum from the packet

    this.uri = `${yamcsBaseUrl}/archive/${this.yamcsInstanceName}/packets/${this.gentime}/${this.seqnum}:extract`;
    return this.extractedPacket = await lastValueFrom(this.http.get<ExtractPacketResponse>(this.uri));
  }

  /**
   * Returns the extracted packet
   * 
   * @returns the extracted packet
   */
  getExtractedPacket() : ExtractPacketResponse {
    return this.extractedPacket;
  }

  unsubscribeFromInstanceHandlerMethod(){
    this.unsubscribeFromInstanceHandler.next('');
  }
}
