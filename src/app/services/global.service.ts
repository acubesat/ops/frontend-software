import { Injectable } from "@angular/core";
import { Router, RouterLink } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class GlobalService {
  constructor(private router: Router) {}

  refreshComponent(path: string) {
    this.router.navigateByUrl("/", { skipLocationChange: true }).then(() => {
      this.router.navigate([`/${path}`]).then(() => {}); //refreshes the component
    });
  }
}
