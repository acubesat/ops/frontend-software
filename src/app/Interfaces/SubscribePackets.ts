import { Subject } from "rxjs";
import { ExtractPacketResponse } from "./ExtractPacketResponse";

export interface SubscribePacketsService {
    packetData: TmPacketReply[];
}

export var subscribePacketsData = new Subject<TmPacketReply>();

export interface SubscribePacketsRequest {

    // Yamcs instance name.
    instance: string;
  
    // Stream name. This is mutually exclusive with the field ``processor``.
    stream: string;
  
    // Processor name. This is mutually exclusive with the field ``stream``.
    processor: string;
}

export interface ListPacketsResponse {
    packet: TmPacketData[];
  
    // Token indicating the response is only partial. More results can then
    // be obtained by performing the same request (including all original
    // query parameters) and setting the ``next`` parameter to this token.
    continuationToken: string;
  }

export interface TmPacketData {
    
    // Raw packet binary
    packet: string;  // Base64
    sequenceNumber: number;
    id: NamedObjectId;

    // When the packet was generated
    generationTime: Date;  // RFC 3339 timestamp

    // When the signal has been received on the groun
    earthReceptionTime: Date;  // RFC 3339 timestamp

    // When the packet was received by Yamcs
    receptionTime: Date;  // RFC 3339 timestamp

    // Name of the Yamcs link where this packet was received from
    link: string;
}

// Used by external clients to identify an item in the Mission Database
// If namespace is set, then the name is that of an alias, rather than
// the qualified name.
export interface NamedObjectId {
    name: string;
    namespace: string;
}

export interface TmPacketReply {
    call: number;
    data: TmData;
    seq: number;
    type: string;
}

export interface TmData {
    "@type": string;
    generationTime: Date;
    id: idData;
    packet: string;
    receptionTime: Date;
}

export interface idData{
    name: string;
}