export interface ExtractPacketResponse {

    // Packet name
    packetName: string;

    // Contained entries
    parameterValues: ExtractedParameterValue[];

    // Messages generated during packet extraction
    messages: string[];
}

// Contents of a container, can be either a parameter entry, or
// a nested container entry.
export interface ExtractedParameterValue {

    // Qualified name of the parameter
    parameter: ParameterInfo;
  
    // Qualified name of the container that defines this
    // specific entry.
    entryContainer: ContainerInfo;
  
    // Absolute location in bits, within the full packet
    location: number;
  
    // Bit length
    size: number;
  
    // Raw value
    rawValue: Value;
  
    // Engineering value
    engValue: Value;
}
  
export interface ParameterInfo {
    name: string;
    qualifiedName: string;
    shortDescription: string;
    longDescription: string;
    alias: NamedObjectId[];
    type: ParameterTypeInfo;
    dataSource: DataSourceType;
    usedBy: UsedByInfo;
    ancillaryData: {[key: string]: any};
  
    // Operations that return aggregate members or array entries
    // may use this field to indicate the path within the parameter.
    path: string[];
}
  
// Used by external clients to identify an item in the Mission Database
// If namespace is set, then the name is that of an alias, rather than
// the qualified name.
export interface NamedObjectId {
    name: string;
    namespace: string;
}
  
export interface ParameterTypeInfo {
    name: string;
    qualifiedName: string;
    shortDescription: string;
    longDescription: string;
    alias: NamedObjectId[];
  
    // Engineering type
    engType: string;
    dataEncoding: DataEncodingInfo;
    unitSet: UnitInfo[];
  
    // Default Alarm, effective when no contextual alarm takes precedence.
    defaultAlarm: AlarmInfo;
    enumValue: EnumValue[];
    absoluteTimeInfo: AbsoluteTimeInfo;
  
    // Contextual alarms
    contextAlarm: ContextAlarmInfo[];
    member: MemberInfo[];
    arrayInfo: ArrayInfo;
    ancillaryData: {[key: string]: any};
  
    // Provides hints on how to format the engineering
    // value as a string.
    numberFormat: NumberFormatTypeInfo;
  
    // True if the engineering type supports signed representation.
    // (only used by integer parameter types)
    signed: boolean;
  
    // String representation of a boolean zero (only used by boolean types)
    zeroStringValue: string;
  
    // String representation of a boolean one (only used by boolean types)
    oneStringValue: string;
  
    // Which parameters this type is used by. This field is only
    // populated when requesting directly a single parameter type.
    usedBy: ParameterInfo[];
}
  
export interface DataEncodingInfo {
    type: Type;
    littleEndian: boolean;
    sizeInBits: number;
    encoding: string;
    defaultCalibrator: CalibratorInfo;
    contextCalibrator: ContextCalibratorInfo[];
}
  
export interface CalibratorInfo {
    polynomialCalibrator: PolynomialCalibratorInfo;
    splineCalibrator: SplineCalibratorInfo;
    javaExpressionCalibrator: JavaExpressionCalibratorInfo;
    type: Type;
}
  
export interface PolynomialCalibratorInfo {
    coefficient: number[];
}
  
export interface SplineCalibratorInfo {
    point: SplinePointInfo[];
}
  
export interface SplinePointInfo {
    raw: number;
    calibrated: number;
}
  
export interface JavaExpressionCalibratorInfo {
    formula: string;
}
  
export interface ContextCalibratorInfo {
    comparison: ComparisonInfo[];
    calibrator: CalibratorInfo;
  
    // This can be used in UpdateParameterRequest to pass a context
    // that is parsed on the server, according to the rules in the
    // excel spreadsheet. Either this or a comparison has to be
    // used (not both at the same time)
    context: string;
}
  
export interface ComparisonInfo {
    parameter: ParameterInfo;
    operator: OperatorType;
    value: string;
    argument: ArgumentInfo;
}
  
export interface ArgumentInfo {
    name: string;
    description: string;
    initialValue: string;
    type: ArgumentTypeInfo;
}
  
export interface ArgumentTypeInfo {
    engType: string;
    dataEncoding: DataEncodingInfo;
    unitSet: UnitInfo[];
  
    // Enumeration states (only used by enumerated arguments)
    enumValue: EnumValue[];
  
    // Minimum value (only used by integer and float arguments)
    rangeMin: number;
  
    // Maximum value (only used by integer and float arguments)
    rangeMax: number;
  
    // Member information (only used by aggregate arguments)
    member: ArgumentMemberInfo[];
  
    // String representation of a boolean zero (only used by boolean arguments)
    zeroStringValue: string;
  
    // String representation of a boolean one (only used by boolean arguments)
    oneStringValue: string;
  
    // Minimum character count (only used by string arguments)
    minChars: number;
  
    // Maximum character count (only used by string arguments)
    maxChars: number;
  
    // True if the engineering type supports signed representation.
    // (only used by integer arguments)
    signed: boolean;
  
    // Minimum byte count (only used by binary arguments)
    minBytes: number;
  
    // Maximum character count (only used by binary arguments)
    maxBytes: number;
  
    // Length of each dimension (only used by array arguments)
    dimensions: ArgumentDimensionInfo[];
  
    // Type of array entries (only used by array arguments)
    elementType: ArgumentTypeInfo;
}
  
export interface UnitInfo {
    unit: string;
}
  
export interface EnumValue {
    value: string;  // String decimal
    label: string;
    description: string;
}
  
export interface ArgumentMemberInfo {
    name: string;
    shortDescription: string;
    longDescription: string;
    alias: NamedObjectId[];
    type: ArgumentTypeInfo;
    initialValue: string;
}
  
export interface ArgumentDimensionInfo {
  
    // Use a fixed integer value. If set, no other options are applicable.
    // This value describes the length.
    fixedValue: string;  // String decimal
  
    // Use the value of the referenced parameter.
    // The value describes the zero-based ending index (length - 1)
    //
    // For a value ``v``, the dimension's length is determined
    // as: ``(v * slope) + intercept``.
    parameter: ParameterInfo;
  
    // Use the value of the referenced argument.
    // The value describes the zero-based ending index (length - 1)
    //
    // For a value ``v``, the dimension's length is determined
    // as: ``(v * slope) + intercept``.
    argument: string;
  
    // Scale the value obtained from a parameter or argument reference.
    slope: string;  // String decimal
  
    // Shift the value obtained from a parameter or argument reference.
    intercept: string;  // String decimal
}
  
export interface AlarmInfo {
    minViolations: number;
    staticAlarmRange: AlarmRange[];
    enumerationAlarm: EnumerationAlarm[];
}
  
export interface AlarmRange {
    level: AlarmLevelType;
    minInclusive: number;
    maxInclusive: number;
    minExclusive: number;
    maxExclusive: number;
}
  
export interface EnumerationAlarm {
    level: AlarmLevelType;
    label: string;
}
  
export interface AbsoluteTimeInfo {
    initialValue: string;
    scale: number;
    offset: number;
    offsetFrom: ParameterInfo;
    epoch: string;
}
  
export interface ContextAlarmInfo {
    comparison: ComparisonInfo[];
    alarm: AlarmInfo;
  
    // This can be used in UpdateParameterRequest to pass a context
    // that is parsed on the server, according to the rules in the
    // excel spreadsheet. Either this or a comparison has to be
    // used (not both at the same time)
    context: string;
}
  
export interface MemberInfo {
    name: string;
    shortDescription: string;
    longDescription: string;
    alias: NamedObjectId[];
    type: ParameterTypeInfo;
}
  
export interface ArrayInfo {
    type: ParameterTypeInfo;
    dimensions: ParameterDimensionInfo[];
}
  
export interface ParameterDimensionInfo {
    fixedValue: string;  // String decimal
    parameter: ParameterInfo;
    slope: string;  // String decimal
    intercept: string;  // String decimal
}
  
export interface NumberFormatTypeInfo {
    numberBase: string;
    minimumFractionDigits: number;
    maximumFractionDigits: number;
    minimumIntegerDigits: number;
    maximumIntegerDigits: number;
    negativeSuffix: string;
    positiveSuffix: string;
    negativePrefix: string;
    positivePrefix: string;
    showThousandsGrouping: boolean;
    notation: string;
}
  
export interface UsedByInfo {
    algorithm: AlgorithmInfo[];
    container: ContainerInfo[];
}
  
export interface AlgorithmInfo {
  
    // Algorithm name
    name: string;
    qualifiedName: string;
    shortDescription: string;
    longDescription: string;
    alias: NamedObjectId[];
    scope: Scope;
  
    // Type of algorithm
    type: Type;
  
    // Language if this is a custom algorithm
    language: string;
  
    // Code if this is a custom algorithm
    text: string;
    inputParameter: InputParameterInfo[];
    outputParameter: OutputParameterInfo[];
    onParameterUpdate: ParameterInfo[];
    onPeriodicRate: string[];  // String decimal
  
    // Operands and operators in Reverse Polish Notation if type ``MATH``.
    mathElements: MathElement[];
}
  
export interface InputParameterInfo {
    parameter: ParameterInfo;
    inputName: string;
    parameterInstance: number;
    mandatory: boolean;
    argument: ArgumentInfo;
}
  
export interface OutputParameterInfo {
    parameter: ParameterInfo;
    outputName: string;
}
  
export interface MathElement {
  
    // Type of element, either an operand kind or an operator.
    type: Type;
  
    // Operator symbol if type ``OPERATOR``.
    operator: string;
  
    // Constant if type ``VALUE_OPERAND``.
    value: number;
  
    // Parameter whose value is used if type ``PARAMETER``.
    parameter: ParameterInfo;
  
    // Parameter instance specifier
    parameterInstance: number;
}
  
export interface ContainerInfo {
    name: string;
    qualifiedName: string;
    shortDescription: string;
    longDescription: string;
    alias: NamedObjectId[];
    maxInterval: string;  // String decimal
    sizeInBits: number;
    baseContainer: ContainerInfo;
    restrictionCriteria: ComparisonInfo[];
    entry: SequenceEntryInfo[];
    usedBy: UsedByInfo;
    ancillaryData: {[key: string]: any};
}
  
export interface SequenceEntryInfo {
    locationInBits: number;
    referenceLocation: ReferenceLocationType;
  
    // For use in sequence containers
    container: ContainerInfo;
    parameter: ParameterInfo;
  
    // For use in command containers
    argument: ArgumentInfo;
    fixedValue: FixedValueInfo;
    repeat: RepeatInfo;
}
  
export interface FixedValueInfo {
    name: string;
    hexValue: string;
    sizeInBits: number;
}
  
export interface RepeatInfo {
    fixedCount: string;  // String decimal
    dynamicCount: ParameterInfo;
    bitsBetween: number;
}
  
// Union type for storing a value
export interface Value {
    type: Type;
    floatValue: number;
    doubleValue: number;
    sint32Value: number;
    uint32Value: number;
    binaryValue: string;  // Base64
    stringValue: string;
    timestampValue: string;  // String decimal
    uint64Value: string;  // String decimal
    sint64Value: string;  // String decimal
    booleanValue: boolean;
    aggregateValue: AggregateValue;
    arrayValue: arrayValue[];
}
  
// An aggregate value is an ordered list of (member name, member value).
// Two arrays are used in order to be able to send just the values (since
// the names will not change)
export interface AggregateValue {
    name: string[];
    value: Value[];
}
  
export enum Type {
    POLYNOMIAL = "POLYNOMIAL",
    SPLINE = "SPLINE",
    MATH_OPERATION = "MATH_OPERATION",
    JAVA_EXPRESSION = "JAVA_EXPRESSION",
}
  
enum OperatorType {
    EQUAL_TO = "EQUAL_TO",
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
    GREATER_THAN = "GREATER_THAN",
    GREATER_THAN_OR_EQUAL_TO = "GREATER_THAN_OR_EQUAL_TO",
    SMALLER_THAN = "SMALLER_THAN",
    SMALLER_THAN_OR_EQUAL_TO = "SMALLER_THAN_OR_EQUAL_TO",
}
  
enum AlarmLevelType {
    NORMAL = "NORMAL",
    WATCH = "WATCH",
    WARNING = "WARNING",
    DISTRESS = "DISTRESS",
    CRITICAL = "CRITICAL",
    SEVERE = "SEVERE",
}
  
enum DataSourceType {
    TELEMETERED = "TELEMETERED",
    DERIVED = "DERIVED",
    CONSTANT = "CONSTANT",
    LOCAL = "LOCAL",
    SYSTEM = "SYSTEM",
    COMMAND = "COMMAND",
    COMMAND_HISTORY = "COMMAND_HISTORY",
    EXTERNAL1 = "EXTERNAL1",
    EXTERNAL2 = "EXTERNAL2",
    EXTERNAL3 = "EXTERNAL3",
    GROUND = "GROUND",
}
  
enum Scope {
    GLOBAL = "GLOBAL",
    COMMAND_VERIFICATION = "COMMAND_VERIFICATION",
    CONTAINER_PROCESSING = "CONTAINER_PROCESSING",
}
  
export enum Type {
    CUSTOM = "CUSTOM",
    MATH = "MATH",
}
  
export enum Type {
    VALUE_OPERAND = "VALUE_OPERAND",
    THIS_PARAMETER_OPERAND = "THIS_PARAMETER_OPERAND",
    OPERATOR = "OPERATOR",
    PARAMETER = "PARAMETER",
}
  
enum ReferenceLocationType {
    CONTAINER_START = "CONTAINER_START",
    PREVIOUS_ENTRY = "PREVIOUS_ENTRY",
}
  
export enum Type {
    FLOAT = "FLOAT",
    DOUBLE = "DOUBLE",
    UINT32 = "UINT32",
    SINT32 = "SINT32",
    INTEGER = "INTEGER",
    BINARY = "BINARY",
    STRING = "STRING",
    TIMESTAMP = "TIMESTAMP",
    UINT64 = "UINT64",
    SINT64 = "SINT64",
    BOOLEAN = "BOOLEAN",
    AGGREGATE = "AGGREGATE",
    ARRAY = "ARRAY",
  
    // Enumerated values have both an integer (sint64Value) and a string representation
    ENUMERATED = "ENUMERATED",
    NONE = "NONE",
}

export interface arrayValue {
    type: string;
    stringValue: string;
    floatValue: number;
    doubleValue: number;
    sint32Value: number;
    uint32Value: number;
    binaryValue: string;  // Base64
    timestampValue: string;  // String decimal
    uint64Value: string;  // String decimal
    sint64Value: string;  // String decimal
    booleanValue: boolean;
    aggregateValue: AggregateValue;
    arrayValue: arrayValue[];
}