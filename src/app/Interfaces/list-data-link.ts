export interface ListDataLink {
    links: Link[]
}
export interface Link {
    instance: string
    name: string
    type: string
    disabled: boolean
    status: string
    detailedStatus: string
    dataInCount: string
    dataOutCount: string
    parameters: string[]
  }