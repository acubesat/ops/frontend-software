import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { InstanceHandlerService } from '../../services/instance-handler.service';
import { DialogData } from '../../app.component';
import { WebsocketService } from 'src/app/services/websocket.service';
import { PacketReceptionService } from 'src/app/services/packet-reception.service';
@Component({
  selector: 'app-change-instance-dialog',
  templateUrl: './change-instance-dialog.component.html',
  styleUrls: ['./change-instance-dialog.component.css']
})
export class ChangeInstanceDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, 
  private instanceHandlerService: InstanceHandlerService,
  public dialogRef: MatDialogRef<ChangeInstanceDialogComponent>,
  private wss: WebsocketService,
  private packets: PacketReceptionService,) { }

  
  ngOnInit(): void {}
  
  onYesClick(): void {
    this.instanceHandlerService.updateInstance(this.data.instance);
    this.dialogRef.close(true);
    this.packets.cancelPacketsConection(); // Call this function to terminate the WebSocket connection
    this.wss.newPacketsRequest();
  }
}
