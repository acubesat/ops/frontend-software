import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataLinksComponent } from './data-links.component';

describe('DataLinksComponent', () => {
  let component: DataLinksComponent;
  let fixture: ComponentFixture<DataLinksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataLinksComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DataLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
