import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { ListDataLink,Link } from 'src/app/Interfaces/list-data-link';
import { Observable, Subscription, lastValueFrom } from "rxjs";
import { WebsocketService } from 'src/app/services/websocket.service';
import { MatTableDataSource } from "@angular/material/table";
import { LinkInfo,subscribeLinkData,LinkEvent } from 'src/app/Interfaces/subscribe-links-data';
import { Router } from '@angular/router';





@Component({
  selector: 'app-data-links',
  templateUrl: './data-links.component.html',
  styleUrls: ['./data-links.component.css']
})
export class DataLinksComponent implements OnInit {
  call!:number;
  subscription:Subscription| undefined;
  extractedLink: LinkInfo[]=[];
  extensionpos!:number;
  extensionFlag:boolean=false;
  datalink!:MatTableDataSource<LinkInfo>;;
  displayedColumns: string[] = ['status', 'name', 'in', 'out','detail'];
  constructor(private httpService: HttpService, private wss:WebsocketService,public router: Router){}



  async ngOnInit(): Promise<void> {
    this.wss.newDataLinkRequest();
    this.wss.listen();
    this.subscription= subscribeLinkData.subscribe(async yamcsreply =>{
      this.extractedLink = yamcsreply.data.links;
      this.datalink= new MatTableDataSource(yamcsreply.data.links); 
      this.call=yamcsreply.call;
      
    });
  
    
         
  }

  ngOnDestroy(){
    this.wss.cancelCall(this.call);
    if(this.subscription) this.subscription.unsubscribe();
  }

  /**
 * Sets the `extensionFlag` to true to display the extension panel
 * and saves the value of `pos` to the `extensionPos` variable.
 * 
 * @param pos The position of the selected link in the array.
 */
  extension(pos:number){
    this.extensionFlag=true;
    this.extensionpos= pos;    
  }

  /**
 * Sends an HTTP request to disable a specific link.
 * 
 * @param linkName The name of the link to be disabled.
 */
  disableLink(linkName:string){
    this.httpService.disableLink(linkName);
    console.log("Disable link function called.");
    }

  /**
 * Send an HTTP request to enable a specific link.
 * @param linkName The name of the link to be enabled
 */
  enableLink(linkName:string){
    this.httpService.enableLink(linkName);
    console.log("Enable link function called.");
    
  }

  /**
 * Sends an HTTP request to reset the counter of a specific link.
 * 
 * @param linkName The name of the link whose counter is being reset.
 */
  resetLink(linkName:string){
    this.httpService.resetLink(linkName);
    console.log("Reset link function called.");
  }

  /**
 * Checks the status of a link and returns the appropriate CSS class for the dot color.
 * 
 * @param link The link object to check for the correct color.
 * @returns An array of strings representing CSS classes.
 */
  dotColor(link:Link){
   let dotlist:string[]=["dot"];
    if(link.status== "UNAVAIL"){
      dotlist.push("error");
    }
    else if(link.status=="DISABLED"){
      dotlist.push("disabled")

    }
    else{
      if(+link.dataInCount>0){
        dotlist.push("activity");
      }
      else{
        dotlist.push("Ok");
      }
    }
    return dotlist;
  }
  
}