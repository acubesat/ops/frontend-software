import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import {
  timeValidation,
  validationStart,
  validationStop,
} from "../../validators/time-validation";
import { Subscription } from 'rxjs';
import { DataserviceService } from 'src/app/services/dataservice.service';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ArchivePacketsService } from 'src/app/services/archive-packets.service';
import { TmPacketData } from 'src/app/Interfaces/SubscribePackets';
import { ExtractPacketService } from 'src/app/services/extract-packet.service';
import { Router } from '@angular/router';
import { ExtractPacketResponse } from 'src/app/Interfaces/ExtractPacketResponse';

@Component({
  selector: 'app-archive-packets',
  templateUrl: './archive-packets.component.html',
  styleUrls: ['./archive-packets.component.css']
})
export class ArchivePacketsComponent implements OnInit, OnDestroy{
  @HostListener('window:beforeunload')
  refresh(){
    this.ngOnDestroy();
  }
  public formFillFields: string[] = ["start", "stop"];
  public starthint: Date = new Date(this.archivePackets.getPacketReceptionTime());
  public stophint: Date = new Date();
  private startedit: string = "";
  private stopedit: string = "";

  archivePacketsForm!: UntypedFormGroup;
  currentStartSubscription!: Subscription;
  currentStopSubscription!: Subscription;
  private start: string = "";
  private stop: string = "";

  archivePacketsArray: TmPacketData[] = [];
  searchTerm: string = '';
  extractedPacket!: ExtractPacketResponse;

  constructor(private fb: UntypedFormBuilder, 
    private dataservice: DataserviceService,
    private snackBar: MatSnackBar,
    private archivePackets: ArchivePacketsService,
    private extract_packet: ExtractPacketService,
    private router: Router) { }

  ngOnInit(): void {
     /**
     * we create the formbuilder group wich will be bind with our input fields for start, stop time and count
     */
    this.archivePacketsForm = this.fb.group(
      {
        start: ["", [Validators.required]],
        stop: ["", [Validators.required]]
      },
      { validator: [timeValidation] }
    );
    this.currentStartSubscription = this.dataservice.currentStart.subscribe((start) => (this.start = start));
    this.currentStopSubscription = this.dataservice.currentStop.subscribe((stop) => (this.stop = stop));
  
    this.archivePacketsForm.get("start")!.setValidators([
      validationStart(new Date(this.start)),
      Validators.required,
    ]);
    this.archivePacketsForm.get("start")!.updateValueAndValidity();
    this.archivePacketsForm.get("stop")!.setValidators([
      validationStop(new Date(this.stop)),
      Validators.required,
    ]);
    this.archivePacketsForm.get("stop")!.updateValueAndValidity();
  }

  openSnackBar(message: string, action: string | undefined): void {
    /**
     * this is the help button in the 3rd step
     */
    this.snackBar.open(message, action, {
      duration: 5000,
      panelClass: "black-snackbar", // color of snackbar
    });
  }

  get startTime() {
    return this.archivePacketsForm.get("start");
  }

  get stopTime() {
    return this.archivePacketsForm.get("stop");
  }

  /**
   * Get values from form and send packet request
   * 
   * @param stepper 
   */
  async createPacketRequest(stepper: MatStepper): Promise<void> {
    /**
     * we implement window alerts in order for the user to know why theres been an error with the time values that they enter
     * also we patch our values to become JSON strings
     */
    this.archivePacketsForm
      .get("start")!
      .patchValue(
        JSON.stringify(this.archivePacketsForm.get("start")!.value)
      );
    this.startedit = this.archivePacketsForm.get("start")!.value;
    this.startedit = this.startedit.replace(/"/g, ""); // The "g" represents the "global modifier". This means that the replace will replace all copies of the matched string with the replacement string you provide.
    this.archivePacketsForm.get("start")!.patchValue(this.startedit);

    this.archivePacketsForm
      .get("stop")!
      .patchValue(JSON.stringify(this.archivePacketsForm.get("stop")!.value));
    this.stopedit = this.archivePacketsForm.get("stop")!.value;
    this.stopedit = this.stopedit.replace(/"/g, "");
    this.archivePacketsForm.get("stop")!.patchValue(this.stopedit);

    this.archivePackets.changeQueries(
      this.archivePacketsForm.get(["start"])!.value,
      this.archivePacketsForm.get(["stop"])!.value
      //passing the start and stop values to the service
    );

    this.archivePacketsArray = await this.archivePackets.sendPacketQuery(this.archivePacketsForm.get(["start"])!.value, this.archivePacketsForm.get(["stop"])!.value);
    stepper.next();
  }

  /**
  * Extracts the packet specified and navigates to the packet-extract component
  * 
  * @param packet 
  */
  async extractPacket(packet: TmPacketData) {
    let packetDate = packet.generationTime;
    this.extractedPacket = await this.extract_packet.exctractPacket(packetDate);
    this.router.navigate(["/packet-extract"]);
  }
  
  searchedPacket(packetName: string): boolean {
    return !!this.searchTerm && packetName.toLowerCase().includes(this.searchTerm.toLowerCase());
  }

  ngOnDestroy(): void {
    if(this.currentStartSubscription) this.currentStartSubscription.unsubscribe();
    if(this.currentStopSubscription) this.currentStopSubscription.unsubscribe();
  }
}
