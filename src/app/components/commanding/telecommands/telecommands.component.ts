import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { MatStepper } from "@angular/material/stepper";
import { MatTableDataSource } from "@angular/material/table";
import { lastValueFrom } from "rxjs";
import { Router } from "@angular/router";
import {
  CommandInfo,
  ListCommandsResponse,
  globalTelecommandsArray,
} from "src/app/Interfaces/Telecommands";
import { DataserviceService } from "src/app/services/dataservice.service";
import { HttpService } from "src/app/services/http.service";

@Component({
  selector: "app-telecommands",
  templateUrl: "./telecommands.component.html",
  styleUrls: ["./telecommands.component.css"],
})
export class TelecommandsComponent implements OnInit {
  telecommandsArray: CommandInfo[] = [];
  telecommandsList!: ListCommandsResponse;
  displayedCol: string[] = ["telecommands"];
  tableDataSource!: MatTableDataSource<CommandInfo>;
  public currentFolder: string = "";
  public commandasyncName: CommandInfo[] = [];
  public firstStep: boolean = false;
  public folders: string[] = [];
  public folderCol: string[] = ["folder"];
  public secondStep: boolean = false;

  constructor(
    private httpService: HttpService,
    private router: Router,
    private cdRef: ChangeDetectorRef
  ) {}

  async ngOnInit(): Promise<void> {
    let spaceSystems = this.httpService.getCommandsFoldersResponse();

    spaceSystems.then((spaceSystems) => (this.folders = spaceSystems));
    this.telecommandsList = await lastValueFrom(this.httpService.getCommands());
    for (let i = 0; i < this.telecommandsList.commands.length; i++) {
      if (!this.telecommandsList.commands[i].abstract) {
        this.telecommandsArray.push(this.telecommandsList.commands[i]);
      }
      globalTelecommandsArray.push(this.telecommandsList.commands[i]);
    }
    this.tableDataSource = new MatTableDataSource(this.telecommandsArray);
  }

  applyFilter(event: Event): void {
    const filteredValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filteredValue.trim().toLowerCase();
  }

  pickTelecommand(stepper: MatStepper) {
    this.secondStep = true;

    this.cdRef.detectChanges();
    //this.onViewChange();
    stepper.next();
  }

  logFolderRowData(row: any, stepper: MatStepper): void {
    this.currentFolder = row;

    this.pickCorrectFolder(row, stepper);
  }

  pickCorrectFolder(row: any, stepper: MatStepper): void {
    /**
     * we pick the folder from logRowData and we populate our variables with the correct telecommands
     * we tell the app to detect the changes that we've made in order to see that the 1st step is true now
     */

    this.commandasyncName = [];
    this.firstStep = true;
    this.cdRef.detectChanges(); // trigger the change detection manually to apply the completed state to the first step, which allows the stepper to move to the next step

    for (let i = 0; i < this.telecommandsArray.length; i++) {
      if (this.telecommandsArray[i] == undefined) {
        //temporary solution
        console.log("Undefined command located.");
      } else if (
        this.telecommandsArray[i].qualifiedName.includes("/" + row + "/")
      ) {
        let parts = this.telecommandsArray[i].qualifiedName.split("/");

        this.commandasyncName = this.commandasyncName.concat(
          this.telecommandsArray[i]
        );
      }
    }

    /**
     * the table data source is used for the search field in the 2nd step */
    this.tableDataSource = new MatTableDataSource(this.commandasyncName);
    /**
     * we tell the stepper to move (first step is true now)
     */

    stepper.next();
  }

  backNavigation(): void {
    this.firstStep = false;

    this.commandasyncName = [];
    this.currentFolder = "";
  }
}
