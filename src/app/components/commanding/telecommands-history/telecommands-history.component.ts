import { literalMap } from "@angular/compiler";
import { Component, HostListener, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { CommandHistoryAttribute } from "src/app/Interfaces/TcHistory";
import { DataserviceService } from "src/app/services/dataservice.service";

@Component({
  selector: "app-telecommands-history",
  templateUrl: "./telecommands-history.component.html",
  styleUrls: ["./telecommands-history.component.css"],
})
export class TelecommandsHistoryComponent implements OnInit, OnDestroy {
  @HostListener('window:beforeunload')
  refresh(){
    this.ngOnDestroy();
  }
  commandHistoryTable!: Array<CommandHistoryAttribute[]>;
  tcVerifierHistoryTable!: Array<CommandHistoryAttribute[]>;
  tcNameArray!: string[];
  tcAssigmentArray!: Array<any[]>;
  tcIdArray!: string[];
  tcTimeArray!: Date[];
  searchTerm: string = '';

  currentTcStatusHistoryArraySubscription!: Subscription;
  currentTcVerifierHistoryArraySubscription!: Subscription;
  tcNameArraySubscription!: Subscription;
  tcAssigmentArraySubscription!: Subscription;
  tcIdArraySubscription!: Subscription;
  tcTimeArraySubscription!: Subscription;

  constructor(private dataService: DataserviceService) {}

  ngOnInit(): void {
    this.currentTcStatusHistoryArraySubscription = this.dataService.currentTcStatusHistoryArray.subscribe(
      (Table) => (this.commandHistoryTable = Table)
    );

    this.currentTcVerifierHistoryArraySubscription = this.dataService.currentTcVerifierHistoryArray.subscribe(
      (Table) => (this.tcVerifierHistoryTable = Table)
    );

    this.tcNameArraySubscription = this.dataService.tcNameArray.subscribe((Name) => (this.tcNameArray = Name));

    this.tcAssigmentArraySubscription = this.dataService.tcAssigmentArray.subscribe(
      (Table) => (this.tcAssigmentArray = Table)
    );

    this.tcIdArraySubscription = this.dataService.tcIdArray.subscribe((id) => (this.tcIdArray = id));

    this.tcTimeArraySubscription = this.dataService.tcTimeArray.subscribe((time) => (this.tcTimeArray = time));
  }

  getValue(assignment: any): any {
    return Object.values(assignment.value)[1];
  }

  searchedTelecommand(command: string): boolean {
    return !!this.searchTerm && command.toLowerCase().includes(this.searchTerm.toLowerCase());
  }

  ngOnDestroy(): void {
    if(this.currentTcStatusHistoryArraySubscription) this.currentTcStatusHistoryArraySubscription.unsubscribe();
    if(this.currentTcVerifierHistoryArraySubscription) this.currentTcVerifierHistoryArraySubscription.unsubscribe();
    if(this.tcNameArraySubscription) this.tcNameArraySubscription.unsubscribe();
    if(this.tcAssigmentArraySubscription) this.tcAssigmentArraySubscription.unsubscribe();
    if(this.tcIdArraySubscription) this.tcIdArraySubscription.unsubscribe();
    if(this.tcTimeArraySubscription) this.tcTimeArraySubscription.unsubscribe();
  }

}
