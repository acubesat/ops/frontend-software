import { Component } from '@angular/core';
import { ExtractPacketResponse, ExtractedParameterValue, Type, arrayValue } from 'src/app/Interfaces/ExtractPacketResponse';
import { ExtractPacketService } from 'src/app/services/extract-packet.service';

@Component({
  selector: 'app-packet-extract',
  templateUrl: './packet-extract.component.html',
  styleUrls: ['./packet-extract.component.css']
})
export class PacketExtractComponent {

  packetExtract!: ExtractPacketResponse;
  public packetData: ExtractedParameterValue[] = [];
  public displayedCol: string[] = ['name', 'type', 'value'];

  constructor(private extractPacket: ExtractPacketService) { }

  ngOnInit(): void {
    this.packetExtract = this.extractPacket.getExtractedPacket();
    this.packetData = this.packetExtract.parameterValues;
    if (this.packetData[this.packetData.length - 1].engValue.type === Type.ARRAY) {
      console.log("Extracted Array Values for ", this.packetExtract.packetName ," :", this.packetData[this.packetData.length - 1].engValue.arrayValue);
    }
  }

  /**
   * Returns the engineering value of the packet depending on the type
   * @param packet 
   * @returns 
   */
  getPacketValues(packet: ExtractedParameterValue): any {
    let values = this.getEngValue(packet);

    if (packet.engValue.type === Type.ARRAY) {
      values = [];
      for (let i = 0; i < packet.engValue.arrayValue.length; i++) {
        values.push(this.getArrayValue(packet.engValue.arrayValue[i]));
      }
    }
    else {
      values = this.getEngValue(packet);
    }
    return values;
  }

  /**
   * Returns the array value of the packet depending on the type, handles array values
   * @param packet 
   * @returns the value of the array object
   */
  getArrayValue(packet: arrayValue): any {
    const arrayValue = packet;

    switch (arrayValue.type) {
      case Type.ENUMERATED:
        return arrayValue.stringValue;
      case Type.UINT32:
        return arrayValue.uint32Value;
      case Type.SINT32:
        return arrayValue.sint32Value;
      case Type.UINT64:
      case Type.SINT64:
        return arrayValue.sint64Value;
      case Type.TIMESTAMP:
        return arrayValue.stringValue;
      case Type.FLOAT:
        return arrayValue.floatValue;
      case Type.DOUBLE:
        return arrayValue.doubleValue;
      case Type.BOOLEAN:
        return arrayValue.booleanValue;
      case Type.STRING:
        return arrayValue.stringValue;
      case Type.ARRAY:
        return arrayValue.arrayValue;
      case Type.AGGREGATE:
        return arrayValue.aggregateValue;
      default:
        return null; // Handle unsupported types
    }
  }

  /**
   * Returns the engineering value of the packet depending on the type
   * 
   * @param packet 
   * @returns the engineering value of the packet
   */
  getEngValue(packet: ExtractedParameterValue): any {
    const engValue = packet.engValue;

    switch (engValue.type) {
      case Type.ENUMERATED:
        return engValue.stringValue;
      case Type.UINT32:
        return engValue.uint32Value;
      case Type.SINT32:
        return engValue.sint32Value;
      case Type.UINT64:
      case Type.SINT64:
        return engValue.sint64Value;
      case Type.TIMESTAMP:
        return engValue.stringValue;
      case Type.FLOAT:
        return engValue.floatValue;
      case Type.DOUBLE:
        return engValue.doubleValue;
      case Type.BOOLEAN:
        return engValue.booleanValue;
      case Type.STRING:
        return engValue.stringValue;
      case Type.ARRAY:
        return engValue.arrayValue;
      case Type.AGGREGATE:
        return engValue.aggregateValue;
      default:
        return null; // Handle unsupported types
    }
  }
}
