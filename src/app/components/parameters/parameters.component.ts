import {
  ChangeDetectorRef,
  Component,
  HostListener,
  OnDestroy,
  OnInit,
} from "@angular/core";
import { MatStepper } from "@angular/material/stepper";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { lastValueFrom } from "rxjs/internal/lastValueFrom";
import { createdRealTimeGraphsInstanceAndQualifiedNames } from "src/app/Interfaces/GraphData";
import {
  ListParametersResponse,
  ParameterInfo,
} from "src/app/Interfaces/ListParameterResponse";
import { HttpService } from "src/app/services/http.service";
import { InstanceHandlerService } from "src/app/services/instance-handler.service";
import { NotificationService } from "src/app/services/notification.service";
import { SharedgraphserviceService } from "src/app/services/sharedgraphservice.service";
import { TelemetryService } from "src/app/services/telemetry.service";
import { WebsocketService } from "src/app/services/websocket.service";

@Component({
  selector: "app-parameters",
  templateUrl: "./parameters.component.html",
  styleUrls: ["./parameters.component.css"],
})
export class ParametersComponent implements OnInit, OnDestroy {
  @HostListener("window:beforeunload")
  refresh() {
    this.ngOnDestroy();
  }
  parametersList!: ListParametersResponse; // includes a list of all parameters (maybe make this a globabal variable)
  displayedCol: string[] = ["Parameter Names", "Parameter Types"];
  tableDataSource!: MatTableDataSource<ParameterInfo>;
  yamcsInstanceName: string = "OBC";
  public currentFolder: string = "";
  public parameterasyncName: ParameterInfo[] = [];
  public firstStep: boolean = false;
  public parameterasync: ParameterInfo[] = [];
  public folders: string[] = [];
  public folderCol: string[] = ["folder"];

  instanceSubscription!: Subscription;

  constructor(
    private instanceHandler: InstanceHandlerService,
    private httpService: HttpService,
    private wss: WebsocketService,
    private sharedService: SharedgraphserviceService,
    private notifService: NotificationService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private telemetryService: TelemetryService
  ) {
    this.instanceSubscription = this.instanceHandler
      .getInstance()
      .subscribe((instanceName) => (this.yamcsInstanceName = instanceName));
  }

  async ngOnInit(): Promise<void> {
    this.parametersList = await lastValueFrom(this.httpService.getParameters());
    this.parametersList.parameters = this.parametersList.parameters.filter(
      (param) => param.hasOwnProperty("type")
    );
    this.tableDataSource = new MatTableDataSource(
      this.parametersList.parameters // TODO: check if this is needed anymore
    );

    let spaceSystems = this.telemetryService.getFoldersResponse();

    spaceSystems.then((spaceSystems) => (this.folders = spaceSystems)); // for dynamic folder

    let parameterResponse = await lastValueFrom(
      this.telemetryService.getTelemetryResponse()
    );

    this.parameterasync = parameterResponse.parameters;
  }

  /**
   * Checks if the graph of a specific parameter is created, by checking if its qualified name and instance
   * exists in the createdRealTimeGraphsQualifiedNames array.
   *
   * If it isn't, we are requesting a wss call to yamcs and also creating the graph,
   * by publishing a click event to the add-graph.directive,
   * which is responsible for the creation of the graph.
   *
   * If it is, we are displaying a notification to the user, saying that the graph
   * already exists.
   *
   * @param qualifiedName the qualified name of the parameter we're requesting
   */

  addGraph(qualifiedName: string): void {
    if (
      !createdRealTimeGraphsInstanceAndQualifiedNames.includes(
        this.yamcsInstanceName + qualifiedName
      )
    ) {
      this.wss.newParameterRequest(qualifiedName);
      this.sharedService.makeGraphPublish(qualifiedName);
      this.router.navigate(["graphs"]);
    } else {
      this.notifService.notification$.next("Graph already exists");
    }
  }

  applyFilter(event: Event): void {
    const filteredValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filteredValue.trim().toLowerCase();
  }

  logFolderRowData(row: any, stepper: MatStepper): void {
    this.currentFolder = row;

    this.pickCorrectFolder(row, stepper);
  }

  pickCorrectFolder(row: any, stepper: MatStepper): void {
    /**
     * we pick the folder from logRowData and we populate our variables with the correct telemetries
     * we tell the app to detect the changes that we've made in order to see that the 1st step is true now
     */

    this.parameterasyncName = [];
    this.firstStep = true;
    this.cdRef.detectChanges(); // trigger the change detection manually to apply the completed state to the first step, which allows the stepper to move to the next step

    for (let i = 0; i < this.parameterasync.length; i++) {
      if (this.parameterasync[i] == undefined) {
        //temporary solution
        console.log("Undefined archive parameter file located.");
      } else if (
        this.parameterasync[i].qualifiedName.includes("/" + row + "/")
      ) {
        this.parameterasyncName = this.parameterasyncName.concat(
          this.parameterasync[i]
        );
      }
    }
    console.log(this.parameterasyncName);
    /**
     * the table data source is used for the search field in the 2nd step */
    this.tableDataSource = new MatTableDataSource(this.parameterasyncName);
    console.log(this.parameterasyncName); //parameterasyncName
    /**
     * we tell the stepper to move (first step is true now)
     */
    stepper.next();
  }

  backNavigation(): void {
    this.firstStep = false;

    this.parameterasyncName = [];
    this.currentFolder = "";
  }

  ngOnDestroy(): void {
    if (this.instanceSubscription) this.instanceSubscription.unsubscribe();
  }
}
