import { Component, OnDestroy, OnInit } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket.service';
import { TmPacketReply } from 'src/app/Interfaces/SubscribePackets';
import { Subscription } from 'rxjs';
import { ExtractPacketService } from 'src/app/services/extract-packet.service';
import { Router } from '@angular/router';
import { ExtractPacketResponse } from 'src/app/Interfaces/ExtractPacketResponse';
import { PacketReceptionService } from 'src/app/services/packet-reception.service';

@Component({
  selector: 'app-telemetry-packet-history',
  templateUrl: './telemetry-packet-history.component.html',
  styleUrls: ['./telemetry-packet-history.component.css']
})
export class TelemetryPacketHistoryComponent implements OnInit,OnDestroy {

  packetDataSubscription: Subscription | undefined;
  public packetData: TmPacketReply[] = [];
  extractedPacket!: ExtractPacketResponse;
  searchTerm: string = '';
  
  constructor( private wss: WebsocketService, private extract_packet: ExtractPacketService, private router: Router,
     private packetReceptionService: PacketReceptionService) {}

  ngOnInit(): void {
    this.packetDataSubscription = this.packetReceptionService.getPacketData().subscribe(data => {
      this.packetData = data.packetData;
      this.packetData = [...this.packetData];
      if(this.packetData.length > 300){
        this.packetData.splice(300); // Limit the number of packets to 300
      }
    });
  }

  /**
   * Extracts the packet specified and navigates to the packet-extract component
   * 
   * @param packet 
   */
  async extractPacket(packet: TmPacketReply) {
    this.extractedPacket = await this.extract_packet.exctractPacket(packet.data.generationTime);
    this.router.navigate(["/packet-extract"]);
  }

  searchedPacket(packetName: string): boolean {
    return !!this.searchTerm && packetName.toLowerCase().includes(this.searchTerm.toLowerCase());
  }

  ngOnDestroy(): void {
    if(this.packetDataSubscription) this.packetDataSubscription.unsubscribe();
  }
}
