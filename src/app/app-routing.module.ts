import { NgModule } from "@angular/core";
import { ExtraOptions, RouterModule, Routes } from "@angular/router";
import { ArchiveTelemetryComponent } from "./components/graphing/archive-telemetry/archive-telemetry.component";
import { TelemetryComponent } from "./components/telemetry/telemetry.component";
import { TelecommandsComponent } from "./components/commanding/telecommands/telecommands.component";
import { TelecommandsHistoryComponent } from "./components/commanding/telecommands-history/telecommands-history.component";
import { TelemetryPacketHistoryComponent } from "./components/telemetry-packet-history/telemetry-packet-history.component";
import { GraphsComponent } from "./components/graphing/graphs/graphs.component";
import { BucketsComponent } from "./components/buckets/buckets.component";
import { ParametersComponent } from "./components/parameters/parameters.component";
import { PacketExtractComponent } from "./components/packet-extract/packet-extract.component";
import { TelecommandItemComponent } from "./components/commanding/telecommand-item/telecommand-item.component";
import { TelecommandResponseComponent } from "./components/commanding/telecommand-response/telecommand-response.component";
import { ArchivePacketsComponent } from "./components/archive-packets/archive-packets.component";
import { DataLinksComponent } from "./components/data-links/data-links.component";

const appRoutes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "",
  },
  { path: "parameters", component: ParametersComponent },
  { path: "telemetry-table", component: TelemetryComponent },
  { path: "archive-telemetry", component: ArchiveTelemetryComponent },
  { path: "graphs", component: GraphsComponent },
  {
    path: "telecommands",
    component: TelecommandsComponent,
    children: [
      {
        path: ":telecommand",
        component: TelecommandItemComponent,
        children: [
          { path: ":response-id", component: TelecommandResponseComponent },
        ],
      },
    ],
  },
  { path: "buckets", component: BucketsComponent },
  { path: "telecommand-history", component: TelecommandsHistoryComponent },
  {
    path: "telemetry-packet-history",
    component: TelemetryPacketHistoryComponent,
  },
  { path: "packet-extract", component: PacketExtractComponent },
  { path: "archive-packets", component: ArchivePacketsComponent},
  { path: "data-links", component: DataLinksComponent}
];
const routerOptions: ExtraOptions = {
  onSameUrlNavigation: "reload", // Set the option to 'reload'
};

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes, routerOptions)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents = [];
