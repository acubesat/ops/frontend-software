FROM node:latest as node 
WORKDIR /frontend-software
COPY . . 
RUN npm install --legacy-peer-deps
RUN npm run build --prod

FROM nginx:alpine 
COPY --from=node /frontend-software/dist/opsmci /usr/share/nginx/html 

# Expose the internal port for your application
EXPOSE 8000

# When building the image, using docker run you need to add the following port flag -p 8000:8000
